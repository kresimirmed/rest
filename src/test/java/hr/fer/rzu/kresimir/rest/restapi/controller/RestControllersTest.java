package hr.fer.rzu.kresimir.rest.restapi.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import hr.fer.rzu.kresimir.rest.restapi.dto.BookDto;
import hr.fer.rzu.kresimir.rest.restapi.dto.Reservation;
import hr.fer.rzu.kresimir.rest.restapi.dto.ReservationInfo;
import hr.fer.rzu.kresimir.rest.restapi.dto.StudentDto;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@WithMockUser(username="user", password="password", roles= {"USER"})
public class RestControllersTest extends AbstractTest {

	// Test Constants for student tests
	private static final String TEST_STUDENT_FIRST_NAME = "Ivan";
	private static final String TEST_STUDENT_LAST_NAME = "Horvat";
	private static final String TEST_STUDENT_UPDATE_VALUE = "Update value";

	// Test Constants for book tests
	private static final String TEST_BOOK_NAME = "Crime and Punishment";
	private static final String TEST_BOOK_AUTOR = "Fyodor Dostoyevsky";
	private static final Integer TEST_BOOK_YEAR = 1866;
	private static final Integer TEST_BOOK_NUMBER_OF_PAGES = 405;
	private static final String TEST_BOOK_UPDATE_VALUE = "updated value";

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	/*********** STUDENTS *****************/

	/**
	 * URL: /students Method: GET
	 *
	 * Number of students 1
	 */
	@Test
	public void _001_getStudentsList() throws Exception {
		String uri = "/students";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		StudentDto[] studentlist = super.mapFromJson(content, StudentDto[].class);

		// Inicijalno je postavljen samo jedan student s ID-em 1 (skripta data.sql)
		assertTrue(studentlist.length == 1);
		assertTrue(studentlist[0].getId().equals(1));
	}

	/**
	 * URL: /students Method: POST
	 */
	@Test
	public void _002_createStudent() throws Exception {
		String uri = "/students";

		// New student
		StudentDto newStudent = new StudentDto();
		newStudent.setFirstName(TEST_STUDENT_FIRST_NAME);
		newStudent.setLastName(TEST_STUDENT_LAST_NAME);

		// Request execute
		String requestBody = super.mapToJson(newStudent);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBody))
				.andReturn();

		// status test
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.CREATED.value(), status);

		// Testing response object
		String content = mvcResult.getResponse().getContentAsString();
		StudentDto studentDto = super.mapFromJson(content, StudentDto.class);

		assertEquals(Integer.valueOf(2), studentDto.getId());
		assertEquals(TEST_STUDENT_FIRST_NAME, studentDto.getFirstName());
		assertEquals(TEST_STUDENT_LAST_NAME, studentDto.getLastName());
		assertNull(studentDto.getEmail());
	}

	/**
	 * URL: /students/{id} Method: PUT
	 */
	@Test
	public void _003_updateStudent() throws Exception {
		String uri = "/students/2";

		StudentDto studentDetails = new StudentDto();
		studentDetails.setFirstName(TEST_STUDENT_UPDATE_VALUE);

		String requestBody = super.mapToJson(studentDetails);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBody))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);
		String content = mvcResult.getResponse().getContentAsString();

		StudentDto updatedStudent = super.mapFromJson(content, StudentDto.class);
		assertEquals(Integer.valueOf(2), updatedStudent.getId());
		assertEquals(TEST_STUDENT_UPDATE_VALUE, updatedStudent.getFirstName());
	}

	/**
	 * URL: /students Method: GET
	 *
	 * Number of students 2
	 */
	@Test
	public void _004_getStudentsList() throws Exception {
		String uri = "/students";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		StudentDto[] studentlist = super.mapFromJson(content, StudentDto[].class);

		// Two students are in database
		assertTrue(studentlist.length == 2);
	}

	/**
	 * URL: /students/1 Method: DELETE
	 */
	@Test
	public void _005_deleteStudent() throws Exception {
		String uri = "/students/1";

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		int status = mvcResult.getResponse().getStatus();

		assertEquals(HttpStatus.OK.value(), status);

		// Not valid ID
		uri = "/students/1";

		mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		status = mvcResult.getResponse().getStatus();

		assertEquals(HttpStatus.NOT_FOUND.value(), status);
	}

	/**
	 * URL: /students/2 Method: GET
	 */
	@Test
	public void _006_getStudentInfo() throws Exception {
		String uri = "/students/2";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		StudentDto studentInfo = super.mapFromJson(content, StudentDto.class);

		// Two students are in database
		assertEquals(Integer.valueOf(2), studentInfo.getId());
		assertEquals(TEST_STUDENT_UPDATE_VALUE, studentInfo.getFirstName());
		assertNull(studentInfo.getLastName());
	}

	/************* BOOKS ***************************/

	/**
	 * URL: /books Method: GET
	 *
	 * Number of books 2
	 */
	@Test
	public void _007_getBooksList() throws Exception {
		String uri = "/books";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		BookDto[] booklist = super.mapFromJson(content, BookDto[].class);

		// Inicijalno je postavljen samo jedan book s ID-em 1 (skripta data.sql)
		assertTrue(booklist.length == 2);
		assertTrue(booklist[0].getId().equals(1));
	}

	/**
	 * URL: /books Method: POST
	 */
	@Test
	public void _008_createBook() throws Exception {
		String uri = "/books";

		// New book
		BookDto newBook = new BookDto();
		newBook.setName(TEST_BOOK_NAME);
		newBook.setAuthor(TEST_BOOK_AUTOR);
		newBook.setYear(TEST_BOOK_YEAR);
		newBook.setNumberOfPages(TEST_BOOK_NUMBER_OF_PAGES);

		// Request execute
		String requestBody = super.mapToJson(newBook);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBody))
				.andReturn();

		// status test
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.CREATED.value(), status);

		// Testing response object
		String content = mvcResult.getResponse().getContentAsString();
		BookDto bookDto = super.mapFromJson(content, BookDto.class);

		assertEquals(Integer.valueOf(3), bookDto.getId());
		assertEquals(TEST_BOOK_NAME, bookDto.getName());
		assertEquals(TEST_BOOK_AUTOR, bookDto.getAuthor());
		assertEquals(TEST_BOOK_YEAR, bookDto.getYear());
		assertEquals(TEST_BOOK_NUMBER_OF_PAGES, bookDto.getNumberOfPages());
	}

	/**
	 * URL: /books/{id} Method: PUT
	 */
	@Test
	public void _009_updateBook() throws Exception {
		String uri = "/books/2";

		BookDto bookDetails = new BookDto();
		bookDetails.setName(TEST_BOOK_UPDATE_VALUE);

		String requestBody = super.mapToJson(bookDetails);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBody))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);
		String content = mvcResult.getResponse().getContentAsString();

		BookDto updatedBook = super.mapFromJson(content, BookDto.class);
		assertEquals(Integer.valueOf(2), updatedBook.getId());
		assertEquals(TEST_BOOK_UPDATE_VALUE, updatedBook.getName());
		assertNull(updatedBook.getAuthor());
	}

	/**
	 * URL: /books Method: GET
	 *
	 * Number of books 3
	 */
	@Test
	public void _010_getBooksList() throws Exception {
		String uri = "/books";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		BookDto[] booklist = super.mapFromJson(content, BookDto[].class);

		// Two books are in database
		assertTrue(booklist.length == 3);
	}

	/**
	 * URL: /books/1 Method: DELETE
	 */
	@Test
	public void _011_deleteBook() throws Exception {
		String uri = "/books/1";

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		int status = mvcResult.getResponse().getStatus();

		assertEquals(HttpStatus.OK.value(), status);

		// Not valid ID
		uri = "/books/1";

		mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		status = mvcResult.getResponse().getStatus();

		assertEquals(HttpStatus.NOT_FOUND.value(), status);
	}

	/**
	 * URL: /books/2 Method: GET
	 */
	@Test
	public void _012_getBookInfo() throws Exception {
		String uri = "/books/2";

		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		BookDto bookInfo = super.mapFromJson(content, BookDto.class);

		// Two books are in database
		assertEquals(Integer.valueOf(2), bookInfo.getId());
		assertEquals(TEST_BOOK_UPDATE_VALUE, bookInfo.getName());
		assertNull(bookInfo.getAuthor());
	}

	/************************ RESERVATIONS ****************************/

	public void _013_StudentId2NoBooks() throws Exception {
		String uri = "/students/2/books";

		// NO BOOKS
		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		BookDto[] booklist = super.mapFromJson(content, BookDto[].class);

		// No books for student with ID 1
		assertTrue(booklist.length == 0);
	}

	@Test
	public void _014_studentGetBooksInvalidId() throws Exception {
		// INVALID ID
		String uri = "/students/4/books";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.NOT_FOUND.value(), status);
	}

	/**
	 * URL: /reservation
	 * Method: PUT
	 *
	 * Number of books 3
	 */
	@Test
	public void _015_getStudentBooksReservation() throws Exception {
		
		String uri = "/reservations";
			
		// CREATE_RESERVATION
		ReservationInfo reservationInfo = new ReservationInfo();
		reservationInfo.setStudentId(2);
		reservationInfo.setBookId(3);

		String requestBody = super.mapToJson(reservationInfo);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBody))
				.andReturn();
		//Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.CREATED.value(), status);

		String content = mvcResult.getResponse().getContentAsString();
		Reservation reservation = super.mapFromJson(content, Reservation.class);

		assertEquals(Integer.valueOf(2), reservation.getStudent().getId());
		assertEquals(Integer.valueOf(3), reservation.getBook().getId());
	}
	
	@Test
	public void _016_bookId3Student() throws Exception {
		String uri = "/books/3/students";

		// NO STUDENTS
		// Request execute
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.OK.value(), status);

		// Testing response objects
		String content = mvcResult.getResponse().getContentAsString();
		StudentDto[] booklist = super.mapFromJson(content, StudentDto[].class);

		// No students for book with ID 1
		assertTrue(booklist.length == 1);
	}

	@Test
	public void _017_bookGetStudentInvalidId() throws Exception {
		// INVALID ID
		String uri = "/books/5/students";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Testing status
		int status = mvcResult.getResponse().getStatus();
		assertEquals(HttpStatus.NOT_FOUND.value(), status);
	}
}
