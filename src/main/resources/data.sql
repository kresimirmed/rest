--BOOKS
INSERT INTO BOOK (ID, NAME, AUTHOR, YEAR, NUMBER_OF_PAGES) VALUES (1, 'Harry Potter and the Philosopher’s Stone', 'J. K. Rowling', 1991 ,223);
INSERT INTO BOOK (ID, NAME, AUTHOR, YEAR, NUMBER_OF_PAGES) VALUES (2, 'Romeo and Juliet', 'William Shakespeare', 1597 ,241);

--STUDENTS
INSERT INTO STUDENT (ID, FIRST_NAME, LAST_NAME, E_MAIL) VALUES (1, 'Pero', 'Perić', 'pero@gmail.com');