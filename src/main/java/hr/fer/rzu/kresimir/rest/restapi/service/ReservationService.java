package hr.fer.rzu.kresimir.rest.restapi.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.rzu.kresimir.rest.restapi.dao.Book;
import hr.fer.rzu.kresimir.rest.restapi.dao.Student;
import hr.fer.rzu.kresimir.rest.restapi.dto.BookDto;
import hr.fer.rzu.kresimir.rest.restapi.dto.Reservation;
import hr.fer.rzu.kresimir.rest.restapi.dto.ReservationInfo;
import hr.fer.rzu.kresimir.rest.restapi.dto.StudentDto;
import hr.fer.rzu.kresimir.rest.restapi.exception.BookNotFoundException;
import hr.fer.rzu.kresimir.rest.restapi.exception.ReservationNotValidException;
import hr.fer.rzu.kresimir.rest.restapi.exception.StudentNotFoundException;

@Service
public class ReservationService {

	@Autowired
	private StudentService studentService;
	@Autowired
	private BookService bookService;

	public Reservation makeReservation(Integer studentId, Integer bookId) {
		Optional<Student> studentOptional = Optional.empty();
		Optional<Book> bookOptional = Optional.empty();

		if (studentId != null)
			studentOptional = studentService.findById(studentId);
		if (bookId != null)
			bookOptional = bookService.findById(bookId);
		
		Student student = studentOptional.orElseThrow(() -> new StudentNotFoundException(studentId));
		Book book = bookOptional.orElseThrow(() -> new BookNotFoundException(bookId));

		student.getBooks().add(book);
		studentService.save(student);

		Reservation reservation = new Reservation();
		reservation.setMessage("Reservation created");
		reservation.setReservationDate(new Date());
		reservation.setBook(new BookDto(book));
		reservation.setStudent(new StudentDto(student));

		return reservation;
	}

	public Reservation makeReservation(ReservationInfo reservationInfo) {
		if(reservationInfo == null)
			throw new ReservationNotValidException();
		
		return makeReservation(reservationInfo.getStudentId(), reservationInfo.getBookId());
	}

}
