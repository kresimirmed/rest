package hr.fer.rzu.kresimir.rest.restapi.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT")
public class Student extends AbstractEntity {

	private  String firstName;
	private  String lastName;
	private String email;

	private List<Book> books = new ArrayList<>();

	@Column(name = "FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@ManyToMany
	@JoinTable(name = "LINK_STUDENT_BOOK", joinColumns = {
			@JoinColumn(name = "ID_STUDENT", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "ID_BOOK", nullable = false, updatable = false) })
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	@Column(name = "E_MAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
