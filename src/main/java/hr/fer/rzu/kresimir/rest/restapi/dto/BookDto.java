package hr.fer.rzu.kresimir.rest.restapi.dto;

import hr.fer.rzu.kresimir.rest.restapi.dao.Book;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookDto extends AbstractDto {
	
	private String name;
	private String author;
	private Integer year;
	private Integer numberOfPages;
	
	public BookDto(Book book) {
		this.id = book.getId();
		this.name = book.getName();
		this.author = book.getAuthor();
		this.year = book.getYear();
		this.numberOfPages = book.getNumberOfPages();
	}

}
