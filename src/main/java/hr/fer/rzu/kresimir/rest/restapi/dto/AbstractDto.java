package hr.fer.rzu.kresimir.rest.restapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class AbstractDto {

	@JsonInclude(Include.NON_NULL)
	protected Integer id;
	
}
