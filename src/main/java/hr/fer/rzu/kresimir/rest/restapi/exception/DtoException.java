package hr.fer.rzu.kresimir.rest.restapi.exception;

public class DtoException extends CustomException{

	private static final long serialVersionUID = -2971942553444972652L;
	
	public DtoException(String message) {
		super(message);
	}
}
