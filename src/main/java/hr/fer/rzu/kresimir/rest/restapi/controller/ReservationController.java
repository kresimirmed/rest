package hr.fer.rzu.kresimir.rest.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rzu.kresimir.rest.restapi.dto.Reservation;
import hr.fer.rzu.kresimir.rest.restapi.dto.ReservationInfo;
import hr.fer.rzu.kresimir.rest.restapi.service.ReservationService;

@RestController
@RequestMapping("reservations")
public class ReservationController {
	
	@Autowired
	private ReservationService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping()
	public Reservation bookReservation(@RequestBody final ReservationInfo reservationInfo) {
		return service.makeReservation(reservationInfo);
	}
}
