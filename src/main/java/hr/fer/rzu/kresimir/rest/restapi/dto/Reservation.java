package hr.fer.rzu.kresimir.rest.restapi.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Reservation {
	
	private String message;
	private StudentDto student;
	private BookDto book;
	@JsonFormat(pattern = "dd.mm.yyyy HH:ss")
	private Date reservationDate;
}
