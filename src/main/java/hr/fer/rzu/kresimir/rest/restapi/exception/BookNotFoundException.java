package hr.fer.rzu.kresimir.rest.restapi.exception;

public class BookNotFoundException extends NotFoundException {

	private static final long serialVersionUID = 5067353054751707714L;

	public BookNotFoundException(String message) {
		super(message);
	}

	public BookNotFoundException( Integer id) {
		this("Book with ID=["+id+"] not found!");
	}
}
