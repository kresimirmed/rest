package hr.fer.rzu.kresimir.rest.restapi.controller.error;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;

@Getter
public class ErrorDetails {
	
	public static final String DESCRIPTION = "ERROR";

	@JsonFormat(pattern = "dd-MM-yyyy hh:ss")
	private Date timestamp;
	private String message;
	private String details;
	private String description = DESCRIPTION;

	public ErrorDetails(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}
}
