package hr.fer.rzu.kresimir.rest.restapi.repositroy;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.fer.rzu.kresimir.rest.restapi.dao.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {
	
	List<Book> findAll();
}
