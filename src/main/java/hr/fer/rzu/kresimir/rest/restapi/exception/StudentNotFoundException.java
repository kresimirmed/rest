package hr.fer.rzu.kresimir.rest.restapi.exception;

public class StudentNotFoundException extends NotFoundException {

	private static final long serialVersionUID = -4801283012356175455L;
	
	public StudentNotFoundException(String message) {
		super(message);
	}
	
	
	public StudentNotFoundException( Integer id) {
		this("Student with ID=["+id+"] not found!");
	}
	
}
