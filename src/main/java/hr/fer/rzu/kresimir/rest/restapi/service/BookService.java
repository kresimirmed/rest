package hr.fer.rzu.kresimir.rest.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.rzu.kresimir.rest.restapi.dao.Book;
import hr.fer.rzu.kresimir.rest.restapi.dto.BookDto;
import hr.fer.rzu.kresimir.rest.restapi.repositroy.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository repository;
	
	/**
	 * Returns all instances of the type.
	 *
	 * @return all entities
	 */
	public List<Book> getAll() {
		return repository.findAll();
	}
	
	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id
	 *            must not be {@literal null}.
	 * @return the entity with the given id or {@literal Optional#empty()} if none
	 *         found
	 * @throws IllegalArgumentException
	 *             if {@code id} is {@literal null}.
	*/
	public Optional<Book> findById(Integer id) {
		return repository.findById(id);
	}
	
	/**
	 * Saves or updates book in DB. If given Book is {@code NULL} than new
	 * book will be created and saved, otherwise book with given ID will be
	 * updated.
	 * 
	 * @param book
	 *            book
	 * @param bookDetails
	 *            details
	 * @return updated book
	 */
	public Book saveOrUpdate(Integer bookId, BookDto bookDetails) {
		Book book = null;
		if (bookId != null) {
			Optional<Book> optional = findById(bookId);
			if (optional.isPresent())
				book = optional.get();
		}

		return saveOrUpdate(book, bookDetails);
	}

	/**
	 * Saves or updates book in DB. If given Book is {@code NULL} than new
	 * book will be created and saved, otherwise book with given ID will be
	 * updated.
	 * 
	 * @param book
	 *            book
	 * @param bookDetails
	 *            details
	 * @return updated book
	 */
	public Book saveOrUpdate(Book book, BookDto bookDetails) {
		if (book == null)
			book = new Book();

		book.setName(bookDetails.getName());
		book.setAuthor(bookDetails.getAuthor());
		book.setYear(bookDetails.getYear());
		book.setNumberOfPages(bookDetails.getNumberOfPages());

		return repository.save(book);
	}
	
	public Book saveOrUpdate(BookDto bookDitels) {
		return saveOrUpdate((Book) null, bookDitels);
	}
	
	public void delete(Book book) {
		repository.delete(book);
	}
}
