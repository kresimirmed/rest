package hr.fer.rzu.kresimir.rest.restapi.dto;

import hr.fer.rzu.kresimir.rest.restapi.dao.Student;
import hr.fer.rzu.kresimir.rest.restapi.exception.DtoException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentDto extends AbstractDto{
	
	private String firstName;
	private String lastName;
	private String email;
	//private List<BookDto> books;
	
	public StudentDto(Student student) {
		if(student == null) throw new DtoException("Unable to create StudentDto. Given Student is NULL");
		
		this.id = student.getId();
		this.firstName = student.getFirstName();
		this.lastName = student.getLastName();
		this.email = student.getEmail();
//		this.books = student.getBooks()
//						.stream()
//						.map(BookDto::new)
//						.collect(Collectors.toList());
	}
	
}
