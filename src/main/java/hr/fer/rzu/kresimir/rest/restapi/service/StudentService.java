package hr.fer.rzu.kresimir.rest.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.rzu.kresimir.rest.restapi.dao.Student;
import hr.fer.rzu.kresimir.rest.restapi.dto.StudentDto;
import hr.fer.rzu.kresimir.rest.restapi.repositroy.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository repository;

	/**
	 * Returns all instances of the type.
	 *
	 * @return all entities
	 */
	public List<Student> getAll() {
		return repository.findAll();
	}

	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id
	 *            must not be {@literal null}.
	 * @return the entity with the given id or {@literal Optional#empty()} if none
	 *         found
	 * @throws IllegalArgumentException
	 *             if {@code id} is {@literal null}.
	*/
	public Optional<Student> findById(Integer id) {
		return repository.findById(id);
	}

	/**
	 * Saves or updates student in DB. If given Student is {@code NULL} than new
	 * student will be created and saved, otherwise student with given ID will be
	 * updated.
	 * 
	 * @param student
	 *            student
	 * @param studentDetails
	 *            details
	 * @return updated student
	 */
	public Student saveOrUpdate(Integer studentId, StudentDto studentDetails) {
		Student student = null;
		if (studentId != null) {
			Optional<Student> optional = findById(studentId);
			if (optional.isPresent())
				student = optional.get();
		}

		return saveOrUpdate(student, studentDetails);
	}

	/**
	 * Saves or updates student in DB. If given Student is {@code NULL} than new
	 * student will be created and saved, otherwise student with given ID will be
	 * updated.
	 * 
	 * @param student
	 *            student
	 * @param studentDetails
	 *            details
	 * @return updated student
	 */
	public Student saveOrUpdate(Student student, StudentDto studentDetails) {
		if (student == null)
			student = new Student();

		student.setFirstName(studentDetails.getFirstName());
		student.setLastName(studentDetails.getLastName());
		student.setEmail(studentDetails.getEmail());

		return repository.save(student);
	}
	
	public Student saveOrUpdate(StudentDto studentDitels) {
		return saveOrUpdate((Student) null, studentDitels);
	}
	
	public void delete(Student student) {
		repository.delete(student);
	}

	public void save(Student student) {
		repository.save(student);
	}
}
