package hr.fer.rzu.kresimir.rest.restapi.conf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class SwaggerHome {
	  @RequestMapping("/api")
	    public RedirectView home() {
	        return new RedirectView("swagger-ui.html");
	    }
}
