package hr.fer.rzu.kresimir.rest.restapi.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rzu.kresimir.rest.restapi.dao.Book;
import hr.fer.rzu.kresimir.rest.restapi.dto.BookDto;
import hr.fer.rzu.kresimir.rest.restapi.dto.StudentDto;
import hr.fer.rzu.kresimir.rest.restapi.exception.BookNotFoundException;
import hr.fer.rzu.kresimir.rest.restapi.exception.NotValidRequestBodyException;
import hr.fer.rzu.kresimir.rest.restapi.service.BookService;

@RestController
@RequestMapping("books")
public class BooksControler {
	
	@Autowired
	private BookService service;
	
	@ResponseStatus(code=HttpStatus.OK)
	@GetMapping(value = "", produces = "application/json")
	public List<BookDto> getBooks() {
		List<Book> books = service.getAll();
		
		return books.stream()
				.map(BookDto::new)
				.collect(Collectors.toList());
	}
	
	@ResponseStatus(code=HttpStatus.OK)
	@GetMapping("/{id}")
	public BookDto getBookById(@PathVariable(value = "id") final Integer id) {
		Book book = service.findById(id)
				.orElseThrow(() -> new BookNotFoundException(id));
		
		return new BookDto(book);
	}

	@ResponseStatus(code=HttpStatus.CREATED)
	@PostMapping("")
	public BookDto createBook(@RequestBody BookDto bookDto) {
		if(bookDto == null) throw new NotValidRequestBodyException("Given request body is null");
		
		Book newBook = service.saveOrUpdate(bookDto);
		
		return new BookDto(newBook);
	}

	@ResponseStatus(code=HttpStatus.OK)
	@PutMapping("/{id}")
	public BookDto updateBook(@PathVariable(value = "id") Integer id, @Valid @RequestBody BookDto bookDetails) {
		Book book = service.saveOrUpdate(id, bookDetails);
		
		return new BookDto(book);
	}

	@ResponseStatus(code=HttpStatus.OK)
	@DeleteMapping("/{id}")
	public ResponseEntity<Book> deleteBook(@PathVariable(value = "id") Integer id) {
		Book book = service.findById(id).orElseThrow(() -> new BookNotFoundException(id));
		service.delete(book);
		
		return ResponseEntity.ok().build();
	}
	
	@ResponseStatus(code=HttpStatus.OK)
	@GetMapping("/{id}/students")
	public List<StudentDto> getBookBooks(@PathVariable(value = "id") final Integer id) {
		Book book = service.findById(id)
				.orElseThrow(() -> new BookNotFoundException(id));
		
		return book.getStudents()
				.stream()
				.map(StudentDto::new)
				.collect(Collectors.toList());
	}

}
