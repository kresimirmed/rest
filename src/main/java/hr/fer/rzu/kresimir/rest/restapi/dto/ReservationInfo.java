package hr.fer.rzu.kresimir.rest.restapi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ReservationInfo {

	@NonNull private Integer studentId;
	@NonNull private Integer bookId;
	
}
