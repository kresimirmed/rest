package hr.fer.rzu.kresimir.rest.restapi.repositroy;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import hr.fer.rzu.kresimir.rest.restapi.dao.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>{
	List<Student> findAll();
}
