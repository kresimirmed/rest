package hr.fer.rzu.kresimir.rest.restapi.exception;

import hr.fer.rzu.kresimir.rest.restapi.exception.CustomException;

public class ReservationNotValidException extends CustomException {

	private static final long serialVersionUID = 6842978230496433274L;
	
	public ReservationNotValidException() {
		super("Reservation is not valid");
	}
}
