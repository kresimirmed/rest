package hr.fer.rzu.kresimir.rest.restapi.exception;

public class NotValidRequestBodyException extends CustomException {

	private static final long serialVersionUID = -367023696116086287L;
	
	public NotValidRequestBodyException(String message) {
		super(message == null ?"Request body not valid!":message);
	}
}
