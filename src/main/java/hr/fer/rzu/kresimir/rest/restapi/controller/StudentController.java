package hr.fer.rzu.kresimir.rest.restapi.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rzu.kresimir.rest.restapi.dao.Student;
import hr.fer.rzu.kresimir.rest.restapi.dto.BookDto;
import hr.fer.rzu.kresimir.rest.restapi.dto.StudentDto;
import hr.fer.rzu.kresimir.rest.restapi.exception.NotValidRequestBodyException;
import hr.fer.rzu.kresimir.rest.restapi.exception.StudentNotFoundException;
import hr.fer.rzu.kresimir.rest.restapi.service.StudentService;

@RestController
@RequestMapping("students")
public class StudentController {

	@Autowired
	private StudentService service;
	
	@ResponseStatus(code=HttpStatus.OK)
	@GetMapping(value = "", produces = "application/json")
	public List<StudentDto> getStudents() {
		List<Student> students = service.getAll();
		
		return students.stream()
				.map(StudentDto::new)
				.collect(Collectors.toList());
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/{id}")
	public StudentDto getStudentById(@PathVariable(value = "id") final Integer id) {
		Student student = service.findById(id)
				.orElseThrow(() -> new StudentNotFoundException(id));
		
		return new StudentDto(student);
	}

	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("")
	public StudentDto createStudent(@RequestBody StudentDto studentDto) {
		if(studentDto == null) throw new NotValidRequestBodyException("Given request body is null");
		
		Student newStudent = service.saveOrUpdate(studentDto);
		
		return new StudentDto(newStudent);
	}

	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/{id}")
	public StudentDto updateStudent(@PathVariable(value = "id") Integer id, @Valid @RequestBody StudentDto studentDetails) {
		Student student = service.saveOrUpdate(id, studentDetails);
		
		return new StudentDto(student);
	}

	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/{id}")
	public ResponseEntity<Student> deleteStudent(@PathVariable(value = "id") Integer id) {
		Student student = service.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
		service.delete(student);
		
		return ResponseEntity.ok().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/{id}/books")
	public List<BookDto> getStudentBooks(@PathVariable(value = "id") final Integer id) {
		Student student = service.findById(id)
				.orElseThrow(() -> new StudentNotFoundException(id));
		
		return student.getBooks()
				.stream()
				.map(BookDto::new)
				.collect(Collectors.toList());
	}
	
	
}
